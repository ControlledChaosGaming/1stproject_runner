﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.forward * Time.deltaTime * 5f);

		if (Input.GetKeyDown (KeyCode.RightArrow) == true) 
		{
			MoveRight ();
		}
		else if (Input.GetKeyDown (KeyCode.LeftArrow) == true) 
		{
			//MoveLeft ();
			StartCoroutine (ml ());
		}
	}

	IEnumerator ml()
	{
		if (transform.position.x == 2) { 
			while (transform.position.x > 0) {
				transform.position = Vector3.MoveTowards (transform.position, (new Vector3 (0, transform.position.y, transform.position.z)), 20f * Time.deltaTime);
				yield return new WaitForSeconds (0f);
			}
		} else if (transform.position.x == 0) {
			while (transform.position.x > -2) {
				transform.position = Vector3.MoveTowards (transform.position, (new Vector3 (-2, transform.position.y, transform.position.z)), 20f * Time.deltaTime);
				yield return new WaitForSeconds (0f);
			}
		}else if (transform.position.x == -2f) {
			while (transform.position.x > -3.45f){// && transform.rotation.z > -45f) {
				transform.position = Vector3.MoveTowards (transform.position, (new Vector3 (-3.45f, 1.5f, transform.position.z)), 20f * Time.deltaTime);
				//rotation.eulerAngles = new Vector3(0,0,-45);//Lerp (transform.rotation, new Quaternion (0, 0, -45), 20f * Time.deltaTime);
				Quaternion rot = transform.rotation;
				rot.eulerAngles = new Vector3 (0, 0, -45f);
				transform.rotation = rot;
			}
		}	
	}

	public void MoveLeft()//moves to left lane
	{
		
		if (transform.position.x == 2) 
		{
			StartCoroutine (ml ());
		}
		else if (transform.position.x == 0) 
		{
			Vector3 newPosition = transform.position;
			newPosition.x = -2;
			transform.position = newPosition;
		}
	}

	public void MoveRight()//moves to right lane
	{
		if (transform.position.x == -2) 
		{
			Vector3 newPosition = transform.position;
			newPosition.x = 0;
			transform.position = newPosition;
		} 
		else if (transform.position.x == 0) 
		{
			Vector3 newPosition = transform.position;
			newPosition.x = 2;
			transform.position = newPosition; 
		}
	}
}
