﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Script_RP_CoinCollectorNGrounded : MonoBehaviour {

	public int coins;

	public Text coinText;

	public Rigidbody rbCharacter;

	public bool isGroundedScript;
	public bool wallRun = false;

	public Script_RP_PlayerBehaviour player;
	public Script_RP_AnimationTriggers animationTriggers;

	public GameObject Obstacle;

	// Use this for initialization
	void Start () {
		coins = PlayerPrefs.GetInt ("coinsCollected");
		coinText.text = "Coins: " + coins;
		isGroundedScript = true;
		player = GameObject.Find ("Character").GetComponent<Script_RP_PlayerBehaviour> ();
	}

	void OnTriggerEnter(Collider col) //collider is low because it wont collide with anything else except for the coin collider
	{
		if (col.gameObject.tag == "Coin") 
		{
			coins += 1;
			PlayerPrefs.SetInt ("coinsCollected", coins);//saving the coins collected uptil now from game start
			coinText.text = "Coins: " + coins;
			Destroy (col.gameObject);
		}

		if (col.gameObject.tag == "Ground")// checking if the player is on the ground
		{
			isGroundedScript = true;
			animationTriggers.animations.SetBool ("isGrounded", true);
			player.transform.rotation = Quaternion.Euler (0, 0, 0);
			animationTriggers.animations.SetBool ("onSlope", false);
			animationTriggers.animations.SetBool ("wallRunJump", false);
			Debug.Log("wallRunJump: " + animationTriggers.animations.GetBool ("wallRunJump"));
		}

		if (col.gameObject.name == "Obstacle") 
		{
			Debug.Log ("End");
			StartCoroutine (DeathTimer());
		}

		if (col.gameObject.name == "PitObstacle") //temporary if the person falls then
		{
			Destroy (GameObject.Find ("SlidingAnim"));
		}

		if (col.gameObject.tag == "Slope") 
		{
			player.transform.rotation = Quaternion.Euler (40, 0, 0);
			animationTriggers.animations.SetBool ("onSlope", true);
			animationTriggers.OnSlopeTrigger ();
		}

		if (col.gameObject.tag == "Wall") 
		{
			player.speed = 0;
			animationTriggers.animations.SetBool ("isGrounded", false);
			animationTriggers.animations.SetBool ("onWall", true);
		}

		if (col.gameObject.tag == "WallRunTrigger")
		{
			
			if (isGroundedScript == false)
			{
				wallRun = false;
			}
			else
			{
				wallRun = true;
			}
		}
	}

	IEnumerator DeathTimer()
	{
		yield return new WaitForSeconds (0.25f);
		Obstacle.SetActive (true);
		Time.timeScale = 0;
	}

	void OnTriggerStay(Collider col)
	{
		if (col.gameObject.tag == "Wall") 
		{
			player.speed = 0;
			animationTriggers.animations.SetBool ("isGrounded", false);
		}

		if (col.gameObject.tag == "WallRunTrigger") {
			if (isGroundedScript == true) 
			{
				wallRun = true;
			}
		}
	}

	void OnTriggerExit(Collider col)// checking if the player is not on the ground
	{
		if (col.gameObject.tag == "Ground") 
		{
			isGroundedScript = false;
			animationTriggers.animations.SetBool ("isGrounded", false);
			Debug.Log ("isGrounded: " + isGroundedScript);
		}

		if (col.gameObject.tag == "Wall") 
		{
			player.speed = 10;
			animationTriggers.animations.SetBool ("onWall", false);
			rbCharacter.drag = 0;
			wallRun = false;
		}

		if (col.gameObject.tag == "WallRunTrigger") 
		{
			wallRun = false;
			if (player.transform.position.x == -3.45f) 
			{
				Debug.Log ("wallruntriggerexit");
				player.FromWall();
				wallRun = false;
			}
		}
	}
}