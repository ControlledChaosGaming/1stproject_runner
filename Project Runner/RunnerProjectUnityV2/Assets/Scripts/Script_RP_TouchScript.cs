﻿using UnityEngine;
using System.Collections;

public class Script_RP_TouchScript : MonoBehaviour {

	public float maxTime = 0.5f;
	public float minSwipeDist = 50;

	Vector3 startPos;
	Vector3 endPos;
	float startTime;
	float endTime;
	float swipeDistance;
	float swipeTime;

	Script_RP_PlayerBehaviour player;

	// Use this for initialization
	void Start () {
		player = GameObject.Find ("Character").GetComponent<Script_RP_PlayerBehaviour>();
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.touchCount > 0) {
			Touch touch = Input.GetTouch (0);
			if (touch.phase == TouchPhase.Began) {
				startTime = Time.time;
				startPos = touch.position;
			}
			else if(touch.phase == TouchPhase.Ended){
				endTime = Time.time;
				endPos = touch.position;

				swipeDistance = (endPos - startPos).magnitude;
				swipeTime = endTime - startTime;

				if (swipeTime < maxTime && swipeDistance > minSwipeDist)
					swipe ();
			}
		}
	}

	void swipe(){
		Vector2 distance = endPos - startPos;
		if (Mathf.Abs (distance.x) > Mathf.Abs (distance.y)) {
			Debug.Log ("Horizontal Swipe");
			if (distance.x > 0) { // do something when player swipes right
				player.MoveRight ();
				Debug.Log ("Right Swipe");
			}
			if (distance.x < 0) { // do something when player swipes left
				player.MoveLeft (); 
				Debug.Log ("Left Swipe");
			}
		} 

		else if (Mathf.Abs (distance.y) > Mathf.Abs (distance.x)) {
			Debug.Log ("Vertical Swipe");
			if (distance.y > 0) { // do something when player swipes up
				player.JumpUp ();
				Debug.Log ("Up Swipe");
			}
			if (distance.y < 0) { // do something when player swipes down
				player.JumpDown ();
				Debug.Log ("Down Swipe");
			}	
		}
	}
}