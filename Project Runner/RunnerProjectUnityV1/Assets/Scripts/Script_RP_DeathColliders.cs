﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_RP_DeathColliders : MonoBehaviour {

	public GameObject ragdoll;
	public GameObject player;
	public float timeRemaining = 2f;

	// Use this for initialization
	void Start ()
	{
		player = GameObject.Find ("SlidingAnim");
	}
	
	// Update is called once per frame
	void Update () 
	{

	}

	void OnTriggerEnter (Collider col)
	{
		if (col.gameObject.tag == "Obstacle" && GameObject.Find("RagdollCharacterOriginal(Clone)") == null) 
		{
			Destroy (player.gameObject);
			Ragdoll r = (Instantiate (ragdoll, player.transform.position, player.transform.rotation) as GameObject).GetComponent<Ragdoll> ();
			r.CopyPose (transform);
		}
	}
}
