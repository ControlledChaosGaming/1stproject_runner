﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using admob;

public class AdManager : MonoBehaviour {

	public int adsShow = 0;

	public static AdManager Instance{ set; get;}

	public string bannerId;
	public string videoId;

	void Start ()
	{
		Instance = this;
		DontDestroyOnLoad (gameObject);

		Admob.Instance ().initAdmob (bannerId,videoId);
		Admob.Instance().setTesting(true);
		Admob.Instance ().loadInterstitial ();
	}

	public void AdsShow (int adShowIncrement)
	{
		adsShow += adShowIncrement;
	}

	public void ShowBanner ()
	{
		//if (adsShow >= 10) {
			Debug.Log ("adsShow" + adsShow);
			Admob.Instance ().showBannerRelative (AdSize.SmartBanner, AdPosition.BOTTOM_CENTER, 0);
		//}
	}

	public void HideBanner ()
	{
		//if (adsShow >= 10) {
			Debug.Log ("adsShow" + adsShow);
			Admob.Instance ().removeAllBanner ();
		//}
	}

	public void ShowVideo ()
	{
		//if (adsShow >= 10) {
			Debug.Log ("adsShow" + adsShow);
			if (Admob.Instance ().isInterstitialReady ()) {
				Admob.Instance ().showInterstitial ();
			}
		//}
	}
}
