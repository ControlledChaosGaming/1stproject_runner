﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_RP_SceneChange : MonoBehaviour {

	public string levelName;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnTriggerEnter (Collider col)
	{
		if (col.gameObject.tag == "Player") 
		{
			Application.LoadLevel (levelName);
			GameObject.Find ("Admob").GetComponent<AdManager> ().AdsShow (4);
		}
	}
}
