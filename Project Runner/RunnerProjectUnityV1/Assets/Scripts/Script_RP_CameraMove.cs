﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_RP_CameraMove : MonoBehaviour {

	public GameObject camera;

	Script_RP_PlayerBehaviour player;

	// Use this for initialization
	void Start () {
		player = GameObject.Find ("Character").GetComponent<Script_RP_PlayerBehaviour> ();
	}
	
	// Update is called once per frame
	void Update () {
		camera.transform.Translate (Vector3.forward * player.speed * Time.deltaTime);
	}
}
