﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_RP_TitleButtons : MonoBehaviour {

	public GameObject titleCanvas;
	public GameObject loadoutCanvas;
	public GameObject loadouts;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void StartBtn()
	{
		titleCanvas.SetActive (false);
		loadoutCanvas.SetActive (true);
		loadoutCanvas.SetActive (false);
		loadoutCanvas.SetActive (true);
		loadouts.SetActive (true);
		loadouts.SetActive (false);
		loadouts.SetActive (true);
	}

	public void Highjump (int highJumpNo)
	{
		PlayerPrefs.SetInt ("highJumpNo", highJumpNo);
	}

	public void MidJump (int midJumpNo)
	{
		PlayerPrefs.SetInt ("midJumpNo", midJumpNo);
	}

	public void PowerSlide (int powerSlideNo)
	{
		PlayerPrefs.SetInt ("powerSlideNo", powerSlideNo);
	}

	public void StartGameBtn()
	{
		Application.LoadLevel ("TestLevel");
	}

	public void RestartFromMainGameBtn()
	{
		Application.LoadLevel (Application.loadedLevel);
		Time.timeScale = 1f;
		GameObject.Find ("Admob").GetComponent<AdManager> ().AdsShow (2);
		AdManager.Instance.HideBanner ();
	}
}
