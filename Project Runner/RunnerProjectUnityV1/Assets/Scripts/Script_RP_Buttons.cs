﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_RP_Buttons : MonoBehaviour {

	public GameObject hud;
	public GameObject pauseMenu;
	public GameObject loadoutWarning;

	// Use this for initialization
	void Start () {
	}

	public void Pause()
	{
		pauseMenu.SetActive (true);
		hud.SetActive (false);
		Time.timeScale = 0f;
		AdManager.Instance.ShowBanner ();
	}

	public void Resume()
	{	
		hud.SetActive (true);
		pauseMenu.SetActive (false);
		Time.timeScale = 1f;
		AdManager.Instance.HideBanner ();
	}

	public void ExitToMainMenu()
	{
		Application.LoadLevel ("Title");
	}

	public void LoadoutWarning ()
	{
		loadoutWarning.SetActive (true);
		pauseMenu.SetActive (false);
	}

	public void NoBtnWarningLoadout()
	{
		pauseMenu.SetActive(true);
		loadoutWarning.SetActive (false);
	}

	public void Title_Start()
	{
		
	}

	public void Title_SoundToggle()
	{
		
	}

	public void Title_Settings()
	{
		
	}
}
