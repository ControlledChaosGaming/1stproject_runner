﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdManager : MonoBehaviour 
{
	public static AdManager Instance{ set; get;}

	void Start()
	{
		Instance = this;
		DontDestroyOnLoad (gameObject);
	}
}
