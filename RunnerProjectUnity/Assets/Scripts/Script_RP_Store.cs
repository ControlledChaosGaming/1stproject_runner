﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Script_RP_Store : MonoBehaviour {

	public GameObject titleCanvas;
	public GameObject store;
	public GameObject doubleHandVaultUnlock;
	public GameObject frontFlipUnlock;
	public GameObject jumpOverUnlock;
	public GameObject doubleHandVaultUnlockloadout;
	public GameObject jumpOverUnlockloadout;

	public Text coinsCollected;
	int coins;

	// Use this for initialization
	void Start ()
	{
		Time.timeScale = 1;
		int highJumpNo = PlayerPrefs.GetInt ("highJumpNo");
		coins = PlayerPrefs.GetInt ("coinsCollected");
		coinsCollected.text = "" + coins;
		if (PlayerPrefs.GetInt ("DoubleHandVault") == 1) 
		{
			doubleHandVaultUnlock.SetActive (false);
			doubleHandVaultUnlockloadout.SetActive (false);
		}
		if (PlayerPrefs.GetInt ("FrontFlip") == 1) 
		{
			frontFlipUnlock.SetActive (false);
		}
		if (PlayerPrefs.GetInt ("JumpOver") == 1)
		{
			jumpOverUnlock.SetActive (false);
			jumpOverUnlockloadout.SetActive (false);
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	public void Checking()
	{
		titleCanvas.SetActive (false);
		store.SetActive (true);
	}

	public void BackToTitle()
	{
		titleCanvas.SetActive (true);
		store.SetActive (false);
	}

	public void PlayerCoins(){
		coinsCollected.text = "" + coins;
		PlayerPrefs.SetInt ("coinsCollected", coins);
	}

	public void UnlockDoubleHandVault()
	{
		coins -= 40;
		PlayerCoins ();
		PlayerPrefs.SetInt ("DoubleHandVault", 1);
		doubleHandVaultUnlock.SetActive (false);
		doubleHandVaultUnlockloadout.SetActive (false);
	}
	public void DoubleHandVault()
	{
		PlayerPrefs.SetInt ("midJumpNo", 3);
	}

	public void UnlockFrontFlip()
	{
		coins -= 50;
		PlayerCoins();
		PlayerPrefs.SetInt ("FrontFlip", 1);
		frontFlipUnlock.SetActive (false);
	}

	public void FrontFlip()
	{
		PlayerPrefs.SetInt ("midJumpNo", 4);
	}

	public void UnlockJumpOver()
	{
		coins -= 30;
		PlayerCoins ();
		PlayerPrefs.SetInt ("JumpOver", 1);
		jumpOverUnlock.SetActive (false);
		jumpOverUnlockloadout.SetActive (false);
	}

	public void JumpOver()
	{
		PlayerPrefs.SetInt ("highJumpNo", 3);
	}

}
