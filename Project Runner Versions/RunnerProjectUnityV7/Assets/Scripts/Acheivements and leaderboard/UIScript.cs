﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIScript : MonoBehaviour {

	public static UIScript Instance { get; private set; }

	// Use this for initialization
	void Start () {
		Instance = this;
	}

	public void GetPoint()
	{
		ManagerScript.Instance.IncrementCounter ();
	}

	public void Restart()
	{
		ManagerScript.Instance.RestartGame ();
	}

	public void Increment()
	{
		Script_RP_PlayGamesServices.IncrementalAchievement (RunnerProjectResources.achievement_test1incremental, 20);
	}

	public void Unlock()
	{
		Script_RP_PlayGamesServices.UnlockAchievement (RunnerProjectResources.achievement_test2);
	}

	public void ShowAchievements()
	{
		Script_RP_PlayGamesServices.ShowAchievementsUI ();
	}

	public void ShowLeaderboards()
	{
		Script_RP_PlayGamesServices.ShowLeaderboardsUI ();
	}

}
