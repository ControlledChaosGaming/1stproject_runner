﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class Script_RP_Analytics : MonoBehaviour {
	
	public void SwitchForStageAnalytics (string stageName)
	{
		switch (stageName) 
		{
		case "AF1":
			Analytics.CustomEvent ("AF1");
			break;
		case "AF2":
			Analytics.CustomEvent ("AF2");
			break;
		case "AF3":
			Analytics.CustomEvent ("AF3");
			break;
		case "AF4":
			Analytics.CustomEvent ("AF4");
			break;
		case "AF5":
			Analytics.CustomEvent ("AF5");
			break;
		default:
			Debug.Log ("No Stage");
			break;
		}
	}
}
