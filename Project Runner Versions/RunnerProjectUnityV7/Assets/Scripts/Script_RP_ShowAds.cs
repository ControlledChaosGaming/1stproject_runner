using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using admob;
//using GoogleMobileAds.Api;
//using GooglePlayGames;
//using GooglePlayGames.BasicApi;
//using UnityEngine.SocialPlatforms;

public class Script_RP_ShowAds : MonoBehaviour {

//	InterstitialAd interstitial;

	// Use this for initialization
	void Start () {
		Debug.Log ("RequestInterstitialAdsStart");
		RequestInterstitialAds ();
		ShowBannerAd ();
	}

	// Update is called once per frame
	void Update () {

	}

	public void ShowBannerAd()
	{
		Debug.Log ("BannerAds");
//		string adID = "ca-app-pub-5055786756143499/2736917562";

		AdManager.Instance.ShowBanner ();

		//***For Testing in the Device***
//		AdRequest request = new AdRequest.Builder ()
//			.AddTestDevice (AdRequest.TestDeviceSimulator)       // Simulator.
//			.AddTestDevice ("2077ef9a63d2b398840261c8221a0c9b")  // My test device.
//			.Build ();

		//***For Production When Submit App***
		//		AdRequest request = new AdRequest.Builder().Build();

//		BannerView bannerAd = new BannerView (adID, AdSize.SmartBanner, AdPosition.Bottom);
//		bannerAd.LoadAd (request);
//		Debug.Log ("BannerAdsEnd");
	}

	public void RequestInterstitialAds()
	{
		Debug.Log ("RequestInterstitialAds");
		string adID = "ca-app-pub-5055786756143499/5690383963";

		#if UNITY_ANDROID
		string adUnitId = adID;
		#elif UNITY_IOS
		string adUnitId = adID;
		#else
		string adUnitId = adID;
		#endif
		Admob.Instance ().loadInterstitial ();
		// Initialize an InterstitialAd.
//		interstitial = new InterstitialAd (adUnitId);

		//***Test***
//		AdRequest request = new AdRequest.Builder ()
//			.AddTestDevice (AdRequest.TestDeviceSimulator)       // Simulator.
//			.AddTestDevice ("2077ef9a63d2b398840261c8221a0c9b")  // My test device.
//			.Build ();

		//***Production***
		//		AdRequest request = new AdRequest.Builder().Build();

		//Register Ad Close Event
//		interstitial.OnAdClosed += Interstitial_OnAdClosed;

		// Load the interstitial with the request.
// 		interstitial.LoadAd (request);

		Debug.Log ("AD LOADED");
	}

	public void ShowInterstitialAd()
	{
		Debug.Log ("ShowInterstitialAds");
		AdManager.Instance.ShowVideo ();
		//Show Ad
//		if (interstitial.IsLoaded ()) {
//			interstitial.Show ();

			//Stop Sound
			//

//			Debug.Log ("Interstitial");
//		} else {
//			Debug.Log ("Interstitial Not Loaded");
//		}
	}

	private void Interstitial_OnAdClosed(object sender, System.EventArgs e)
	{
		//Resume Play Sound
	}

//	public void Authentications ()
//	{
//		PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder ().Build ();
//		PlayGamesPlatform.InitializeInstance (config);
//		PlayGamesPlatform.DebugLogEnabled = true;
//		PlayGamesPlatform.Activate ();
//
//		SignIn ();
//
//	}

	void SignIn () 
	{
		//Social.localUser.Authenticate (success => {	});
		//if (!Social.localUser.authenticated) {
		Social.localUser.Authenticate ((bool success) => 
		{
		if (success) 
		{
			Debug.Log ("Logged in");
		}
		else
		{
			Debug.Log ("Login Failed");
		}

		});

		//}
	}

}