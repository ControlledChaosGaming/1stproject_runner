﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_RP_TitleButtons : MonoBehaviour {

	public GameObject titleCanvas;
	public GameObject loadoutCanvas;
	public GameObject loadouts;
	public GameObject levelSelector;

	public void LoadoutBtn()
	{
		titleCanvas.SetActive (false);
		loadoutCanvas.SetActive (true);
		loadoutCanvas.SetActive (false);
		loadoutCanvas.SetActive (true);
		loadouts.SetActive (true);
		loadouts.SetActive (false);
		loadouts.SetActive (true);
		levelSelector.SetActive (false);
	}

	public void BackToLevelSelect()
	{
		levelSelector.SetActive (true);
		loadoutCanvas.SetActive (false);
	}

	public void Highjump (int highJumpNo)
	{
		PlayerPrefs.SetInt ("highJumpNo", highJumpNo);
	}

	public void MidJump (int midJumpNo)
	{
		PlayerPrefs.SetInt ("midJumpNo", midJumpNo);
	}

	public void PowerSlide (int powerSlideNo)
	{
		PlayerPrefs.SetInt ("powerSlideNo", powerSlideNo);
	}

	public void StartBtn()
	{
		titleCanvas.SetActive (false);
		levelSelector.SetActive (true);
	}

	public void RestartFromMainGameBtn()
	{
		Application.LoadLevel (Application.loadedLevel);
		Time.timeScale = 1f;
		AdManager.Instance.adsShowVar += 1;
	}

	public void TutorialLevel()
	{
		Application.LoadLevel ("TutorialLevel");
	}

	public void Authentication()
	{
		
	}

	public void Buy10Coins()
	{
		Script_RP_IAP.Instance.Buy10Coins ();
	}

	public void Buy20Coins()
	{
		Script_RP_IAP.Instance.Buy20Coins ();
	}
}
