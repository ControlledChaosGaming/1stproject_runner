﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_RP_AnimationTriggers : MonoBehaviour {

	public Animator animations;
	public Script_RP_CoinCollectorNGrounded coinCollector;

	int highJumpNo;
	int midJumpNo;
	int powerSlideNo;
	Animation anim;
	 
	// Use this for initialization
	void Start () {
		animations = GetComponent<Animator> ();
		anim = GameObject.Find ("Character").GetComponent<Animation> ();
	}

	public void RollTrigger ()
	{
		if (this.animations.GetCurrentAnimatorStateInfo (0).IsName ("RR_Running")) {// if the player is running only then perform action
			animations.SetTrigger ("RollTrigger");
		}
	}

	public void JumpTrigger ()
	{
		if (this.animations.GetCurrentAnimatorStateInfo (0).IsName ("RR_Running") || animations.GetBool("onWall") == true) {// if the player is running only then perform action
			GameObject.Find("Character").GetComponent<Rigidbody> ().AddForce (new Vector3 (0, 300,0));
			animations.SetTrigger ("JumpTrigger");
		}
	}

	public void HighJump ()
	{
		if (this.animations.GetCurrentAnimatorStateInfo (0).IsName ("RR_Running")) // if the player is running only then perform action
		{
			animations.SetBool ("isGrounded", false);
			highJumpNo = PlayerPrefs.GetInt ("highJumpNo");
			switch (highJumpNo) {
			case 1:
				animations.SetTrigger ("BackSomersaultTrigger");
				JumpHeight (440);
				break;
			case 2:
				animations.SetTrigger ("JumpTrigger");
				break;
			case 3: 
				animations.SetTrigger ("JumpOverTrigger");
				JumpHeight(440);
				break;
			}
		}
	}

	public void MidJump ()
	{
		if (this.animations.GetCurrentAnimatorStateInfo (0).IsName ("RR_Running")) // if the player is running only then perform action
		{
			midJumpNo = PlayerPrefs.GetInt ("midJumpNo");
			switch (midJumpNo) {
			case 1:
				animations.SetTrigger ("SingleHandVaultTrigger");
				break;
			case 2:
				animations.SetTrigger ("JumpTrigger");
				break;
			case 3:
				animations.SetTrigger ("DoubleHandVaultTrigger");
				break;
			case 4:
				animations.SetTrigger ("FrontFlipTrigger");
				JumpHeight (250);
				break;
			}
		}
	}

	public void PowerSlide ()
	{
		if (this.animations.GetCurrentAnimatorStateInfo (0).IsName ("RR_Running"))// if the player is running only then perform action
		{
			powerSlideNo = PlayerPrefs.GetInt ("powerSlideNo");
			switch (powerSlideNo) {
			case 1:
				animations.SetTrigger ("SlideTrigger");
				break;
			case 2:
				animations.SetTrigger ("RollTrigger");
				break;
			}
		}
	}

	public void OnSlopeTrigger ()
	{
		animations.SetTrigger("SlopeTrigger");
	}

	public void JumpHeight (int jumpHeight)
	{
		GameObject.Find("Character").GetComponent<Rigidbody> ().AddForce (new Vector3 (0, jumpHeight,0));
	}
}