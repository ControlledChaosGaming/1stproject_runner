﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Script_RP_PlayerBehaviour: MonoBehaviour {

	Script_RP_PlayerBehaviour player;
	Rigidbody rbCharacter;

	public bool checkSwipeDown = false;
	public bool checkSwipeUp = false;
	public bool checkMoveRight = false;
	public bool checkMoveLeft = false;
	public bool checkWallRun = false;

	public int jumpHeight = 300;
	public int speed;

	public Slider playerSpeedSlider;

	public Script_RP_AnimationTriggers animationTriggers;
	public Script_RP_CoinCollectorNGrounded coinCollector;

	// Use this for initialization
	void Start () {
		player = GetComponent<Script_RP_PlayerBehaviour> ();
		animationTriggers = GameObject.Find ("SlidingAnim").GetComponent<Script_RP_AnimationTriggers> ();
		rbCharacter = GetComponent<Rigidbody> ();
		speed = 10;
		playerSpeedSlider = GameObject.Find ("SpeedSlider").GetComponent<Slider> ();
		playerSpeedSlider.value = 10;
		Vector3 pos = transform.position;
		pos.x = 0;
		transform.position = pos;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown(KeyCode.UpArrow))
		{
			JumpUp ();
		}
		if (Input.GetKeyDown (KeyCode.DownArrow)) 
		{
			JumpDown ();
		}

		transform.Translate (Vector3.forward * speed * Time.deltaTime);//moves the player forward

		if (Input.GetKeyDown (KeyCode.RightArrow) == true) 
		{	
			MoveRight ();
		} 
		else if (Input.GetKeyDown (KeyCode.LeftArrow) == true) 
		{
			MoveLeft ();
		}
	}


	////////////////////////////Vertical Swipes//////////////////////////////////////////////////////////////////////

	public void JumpUp()// For Upwards swipe gesture
	{  
		checkSwipeUp = true;

		animationTriggers.JumpTrigger ();
		if (transform.position.x == -3.45f)
		{
			animationTriggers.animations.SetBool ("wallRunJump", true);

			Quaternion rot = player.transform.localRotation;
			rot.eulerAngles = new Vector3 (0, 0, 0);
			player.transform.localRotation = rot;

			Debug.Log ("wallrunexit");

			Vector3 pos = player.transform.position;
			pos.x = -2f;
			player.transform.position = pos;

			Vector3 posCharacter = transform.position;
			posCharacter.y = 1.5f;
			transform.position = posCharacter;

			coinCollector.rbCharacter.useGravity = true;
			coinCollector.rbCharacter.isKinematic = false;
			coinCollector.wallRun = false;
			rbCharacter.AddForce (new Vector3 (0, 400, 0));
		}

		else if (animationTriggers.animations.GetBool ("isGrounded") == false && animationTriggers.animations.GetBool ("onWall") == true) 
		{
			rbCharacter.AddForce (new Vector3 (0, 2500, 0));
			rbCharacter.drag = 20;
			CancelInvoke ();
			InvokeRepeating ("CharacterDrag", 0.1f, 0.1f);
		}
	}
		
	void CharacterDrag(){
		rbCharacter.drag -= 1;
		if (rbCharacter.drag < 0)
		{
			rbCharacter.drag = 0;
			CancelInvoke ();
		}
	}

	public void JumpDown()// For downwards swipe gesture
	{
		checkSwipeDown = true;	
		StartCoroutine (DecrementSpeed ());
		if (transform.position.x != -3.45f) {
			animationTriggers.RollTrigger ();
		}
	}

	IEnumerator DecrementSpeed()
	{
		for (int i = player.speed; i >= 10; --i) {
			player.speed = i;
			player.playerSpeedSlider.value = i;
			yield return new WaitForSeconds (0.05f);
		}
	}

	public void JumpHeight()
	{
		GetComponent<Rigidbody> ().AddForce (new Vector3 (0, jumpHeight, 0));
	}
	////////////////////////////End//////////////////////////////////////////////////////////////////////

	////////////////////////////Horizontal Swipes//////////////////////////////////////////////////////////////////////

	public void MoveLeft()//moves to left lane
	{
		checkMoveLeft = true;
		StartCoroutine(ml());
	}

	IEnumerator ml()//moves to left lane
	{
		if (transform.position.x >= 2) //from x position 2 to 0
		{
			while (transform.position.x > 0) 
			{
				transform.position = Vector3.MoveTowards (transform.position, (new Vector3 (0, transform.position.y, transform.position.z)), 20f * Time.deltaTime);
				yield return new WaitForSeconds (0f);
			}
		} 
		else if (transform.position.x == 0) // from x position 0 to -2 
		{
			while (transform.position.x > -2) 
			{
				transform.position = Vector3.MoveTowards (transform.position, (new Vector3 (-2, transform.position.y, transform.position.z)), 20f * Time.deltaTime);
				yield return new WaitForSeconds (0f);
			}
		} 
		else if (transform.position.x == -2f && coinCollector.wallRun == true && coinCollector.isGroundedScript == true) 
		{
			checkWallRun = true;
			Time.timeScale = 1f;
			while (transform.position.x > -3.45f) 
			{	
				transform.position = Vector3.MoveTowards (transform.position, (new Vector3 (-3.45f, 1.5f, transform.position.z)), 20f * Time.deltaTime);
				yield return new WaitForSeconds (0f);
				//rotation to -45 degree
				Quaternion rot = transform.rotation;
				rot.eulerAngles = new Vector3 (0, 0, -45f);
				transform.rotation = rot;
				coinCollector.rbCharacter.useGravity = false;
				coinCollector.rbCharacter.isKinematic = true;
			}
		}
	}

	public void MoveRight()//moves to right lane
	{
		checkMoveRight = true;
		StartCoroutine(mr());
	}
		
	IEnumerator mr()//moves to right lane
	{
		if (transform.position.x == -2) { // from x position -2 to 0 
			while (transform.position.x < 0) {
				transform.position = Vector3.MoveTowards (transform.position, (new Vector3 (0, transform.position.y, transform.position.z)), 20f * Time.deltaTime);
				yield return new WaitForSeconds (0f);
			}
		} 
		else if (transform.position.x == 0) // from x position 0 to 2
		{
			while (transform.position.x < 2) {
				transform.position = Vector3.MoveTowards (transform.position, (new Vector3 (2, transform.position.y, transform.position.z)), 20f * Time.deltaTime);
				yield return new WaitForSeconds (0f);
			}
		} 
		else if (transform.position.x == -3.45f)
		{			
			//yield return StartCoroutine(Ex());
			//rotation to -45 degree

			while (transform.position.x < -2f) {	
				transform.position = Vector3.MoveTowards (transform.position, (new Vector3 (-2f, 0f, transform.position.z)), 20f * Time.deltaTime);
				yield return new WaitForSeconds (0f);
				//rotation to -45 degree
				Quaternion rot = transform.rotation;
				rot.eulerAngles = new Vector3 (0, 0, 0f);
				transform.rotation = rot;
			}

			ResetRigidbody ();
		}
	}

	public void ResetRigidbody ()//Reset Rigidbody properties for running
	{
		coinCollector.rbCharacter.useGravity = true;
		coinCollector.rbCharacter.isKinematic = false;
		coinCollector.wallRun = false;
	}

	public void FromWall()
	{
		Quaternion rot = transform.localRotation;
		rot.eulerAngles = new Vector3 (0, 0, 0);
		transform.localRotation = rot;
		Debug.Log ("wallrunexit");

		Vector3 pos = transform.position;
		pos.x = -2f;
		transform.position = pos;

		pos = transform.position;
		pos.y = 0f;
		transform.position = pos;

		coinCollector.rbCharacter.useGravity = true;
		coinCollector.rbCharacter.isKinematic = false;
		coinCollector.wallRun = false;
	}
	////////////////////////////End//////////////////////////////////////////////////////////////////////
}
