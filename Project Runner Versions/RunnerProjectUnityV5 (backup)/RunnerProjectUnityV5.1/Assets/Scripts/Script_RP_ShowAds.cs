﻿using System;
using UnityEngine;
using System.Collections;
using GoogleMobileAds;
using GoogleMobileAds.Api;

public class Script_RP_ShowAds : MonoBehaviour
{
	private BannerView bannerView;
	private InterstitialAd interstitial;

	void Start()
	{
		RequestInterstitial();
		ShowBannerAd();
		bannerView.Show();


		//ShowInterstitial();
	}

	public void ShowBannerAd()
	{
		Debug.Log ("Show Banner");

		#if UNITY_EDITOR
		string adUnitId = "unused";
		#elif UNITY_ANDROID
		string adUnitId = "ca-app-pub-5055786756143499/2736917562";
		#elif UNITY_IPHONE
		string adUnitId = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";
		#else
		string adUnitId = "unexpected_platform";
		#endif

		// Create a 320x50 banner at the top of the screen.
		bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Bottom);
		// Register for ad events.
		bannerView.OnAdLoaded += HandleAdLoaded;
		bannerView.OnAdFailedToLoad += HandleAdFailedToLoad;
		bannerView.OnAdLoaded += HandleAdOpened;
		bannerView.OnAdClosed += HandleAdClosed;
		bannerView.OnAdLeavingApplication += HandleAdLeftApplication;
		// Load a banner ad.
		bannerView.LoadAd(createAdRequest());

		Debug.Log ("Show Banner Ends");
		}

		public void RequestInterstitial()
		{
		Debug.Log ("Request Interstitial");

		#if UNITY_EDITOR
		string adUnitId = "unused";
		#elif UNITY_ANDROID
		string adUnitId = "ca-app-pub-5055786756143499/5690383963";
		#elif UNITY_IPHONE
		string adUnitId = "INSERT_IOS_INTERSTITIAL_AD_UNIT_ID_HERE";
		#else
		string adUnitId = "unexpected_platform";
		#endif

		// Create an interstitial.
		interstitial = new InterstitialAd(adUnitId);
		// Register for ad events.
		interstitial.OnAdLoaded += HandleInterstitialLoaded;
		interstitial.OnAdFailedToLoad += HandleInterstitialFailedToLoad;
		interstitial.OnAdOpening += HandleInterstitialOpened;
		interstitial.OnAdClosed += HandleInterstitialClosed;
		interstitial.OnAdLeavingApplication += HandleInterstitialLeftApplication;
		// Load an interstitial ad.
		interstitial.LoadAd(createAdRequest());

		Debug.Log ("Request Interstitial Ends");
		}

		// Returns an ad request with custom ad targeting.
		private AdRequest createAdRequest()
		{
		return new AdRequest.Builder()
		.AddTestDevice(AdRequest.TestDeviceSimulator)
		.AddTestDevice("0123456789ABCDEF0123456789ABCDEF")
		.AddKeyword("game")
		.SetGender(Gender.Male)
		.SetBirthday(new DateTime(1985, 1, 1))
		.TagForChildDirectedTreatment(false)
		.AddExtra("color_bg", "9B30FF")
		.Build();
		}

		public void ShowInterstitialAd()
		{
		Debug.Log ("Show Interstitial Function");
		if (interstitial.IsLoaded())
		{
			Debug.Log ("Loaded");
			interstitial.Show();
		}
		else
		{
			Debug.Log ("Interstitial is not ready yet.");
		}
		Debug.Log ("Show Interstitial Function Ends");
		}

		#region Banner callback handlers

		public void HandleAdLoaded(object sender, EventArgs args)
		{
		//print("HandleAdLoaded event received.");
		}

		public void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
		{
		//print("HandleFailedToReceiveAd event received with message: " + args.Message);
		}

		public void HandleAdOpened(object sender, EventArgs args)
		{
		//print("HandleAdOpened event received");
		}

		void HandleAdClosing(object sender, EventArgs args)
		{
		//print("HandleAdClosing event received");
		}

		public void HandleAdClosed(object sender, EventArgs args)
		{
		//print("HandleAdClosed event received");
		}

		public void HandleAdLeftApplication(object sender, EventArgs args)
		{
		//print("HandleAdLeftApplication event received");
		}

		#endregion

		#region Interstitial callback handlers

		public void HandleInterstitialLoaded(object sender, EventArgs args)
		{
		//print("HandleInterstitialLoaded event received.");
		}

		public void HandleInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs args)
		{
		//print("HandleInterstitialFailedToLoad event received with message: " + args.Message);
		}

		public void HandleInterstitialOpened(object sender, EventArgs args)
		{
		//print("HandleInterstitialOpened event received");
		}

		void HandleInterstitialClosing(object sender, EventArgs args)
		{
		//print("HandleInterstitialClosing event received");
		}

		public void HandleInterstitialClosed(object sender, EventArgs args)
		{
		//print("HandleInterstitialClosed event received");
		}

		public void HandleInterstitialLeftApplication(object sender, EventArgs args)
		{
		//print("HandleInterstitialLeftApplication event received");
		}

		#endregion

		//Load Next Level
		public void NextLevel(string nextLevelToChangeTo)
		{
		//ShowInterstitial();
		Application.LoadLevel(nextLevelToChangeTo);
		StartTime();
		}

		//Reload Current Level
		public void Restart()
		{
		Application.LoadLevel(Application.loadedLevel);
		StartTime();
		}

		//Load Main Menu
		public void MainMenu(string nextLevelToChangeTo)
		{
		Application.LoadLevel(nextLevelToChangeTo);
		StartTime();
		}

		public void StopTime()
		{
		Time.timeScale = 0;
		}

		public void StartTime()
		{
		Time.timeScale = 1;
		}

		}
