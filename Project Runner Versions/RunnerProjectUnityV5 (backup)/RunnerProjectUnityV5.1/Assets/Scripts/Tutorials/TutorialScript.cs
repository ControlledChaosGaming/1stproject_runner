﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialScript : MonoBehaviour {

	Script_RP_PlayerBehaviour pB;

	public GameObject arrowImage;
	public GameObject TutTextGameObject;

	public Text tutorialText;

	Script_RP_PowerButtons powerBtns;

	// Use this for initialization
	void Start () {
		pB = GameObject.Find ("Character").GetComponent<Script_RP_PlayerBehaviour> ();
		powerBtns = GameObject.Find ("ButtonManager").GetComponent<Script_RP_PowerButtons> ();
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnTriggerEnter (Collider col)
	{
		if (col.gameObject.name == "Character") 
		{
			Time.timeScale = 0.05f;
			Debug.Log ("entered");
		}
	}

	void OnTriggerStay (Collider col)
	{
		if (gameObject.name == "RollTutorial" && col.gameObject.name == "Character") 
		{
			arrowImage.transform.eulerAngles = new Vector3 (0, 0, 90);
			arrowImage.SetActive (true);
			tutorialText.text = "Swipe Down to Roll";
			TutTextGameObject.SetActive (true);
			if (pB.checkSwipeDown) 
			{
				TutorialArrownTextDisapper ();
			}
		}

		if (gameObject.name == "JumpTutorial" && col.gameObject.name == "Character") 
		{
			arrowImage.transform.eulerAngles = new Vector3 (0, 0, 270);
			arrowImage.SetActive (true);
			tutorialText.text = "Swipe Up to Jump";
			TutTextGameObject.SetActive (true);
			if (pB.checkSwipeUp == true) 
			{
				TutorialArrownTextDisapper ();
			}
		}

		if (gameObject.name == "RightSwitchLaneTutorial" && col.gameObject.name == "Character") 
		{
			arrowImage.transform.eulerAngles = new Vector3 (0, 0, 180);
			arrowImage.SetActive (true);
			tutorialText.text = "Swipe Right to Change to Right Lane";
			TutTextGameObject.SetActive (true);
			if (pB.checkMoveRight) 
			{
				TutorialArrownTextDisapper ();
			}
		}

		if (gameObject.name == "LeftSwitchLaneTutorial" && col.gameObject.name == "Character") 
		{
			arrowImage.transform.eulerAngles = new Vector3 (0, 0, 0);
			arrowImage.SetActive (true);
			tutorialText.text = "Swipe Left to Change to Left Lane";
			TutTextGameObject.SetActive (true);
			if (pB.checkMoveLeft) 
			{
				TutorialArrownTextDisapper ();
			}
		}

		if (gameObject.name == "HighJumpTutorial" && col.gameObject.name == "Character") 
		{
			arrowImage.transform.eulerAngles = new Vector3 (0, 0, 180);
			arrowImage.SetActive (true);
			tutorialText.text = "A special high jump button";
			TutTextGameObject.SetActive (true);
			if (powerBtns.highJumpTutorial) 
			{
				TutorialArrownTextDisapper ();
			}
		}

		if (gameObject.name == "MidJumpTutorial" && col.gameObject.name == "Character") 
		{
			arrowImage.transform.eulerAngles = new Vector3 (0, 0, 180);
			arrowImage.SetActive (true);
			tutorialText.text = "A special mid jump button";
			TutTextGameObject.SetActive (true);
			if (powerBtns.midJumpTutorial) 
			{
				TutorialArrownTextDisapper ();
			}
		}

		if (gameObject.name == "PowerSlideTutorial" && col.gameObject.name == "Character") 
		{
			arrowImage.transform.eulerAngles = new Vector3 (0, 0, 180);
			arrowImage.SetActive (true);
			tutorialText.text = "A special power slide button";
			TutTextGameObject.SetActive (true);
			if (powerBtns.powerSlideTutorial) 
			{
				TutorialArrownTextDisapper ();
			}
		}

		if (gameObject.name == "WallRunTutorial" && col.gameObject.name == "Character") 
		{
			arrowImage.transform.eulerAngles = new Vector3 (0, 0, 0);
			arrowImage.SetActive (true);
			tutorialText.text = "When there is a wall, You can wall run your way past the obstacle";
			TutTextGameObject.SetActive (true);
			StartCoroutine (StopWallRunTut ());
		}

		if (gameObject.name == "SpeednComboTutorial" && col.gameObject.name == "Character") 
		{
			arrowImage.transform.eulerAngles = new Vector3 (0, 0, 180);
			arrowImage.transform.position = new Vector3 (1200, 925, 0);
			arrowImage.SetActive (true);
			TutTextGameObject.transform.position = new Vector3 (1200, 800, 0);
			tutorialText.text = "Higher the speed and combos, the more score you yield";
			TutTextGameObject.SetActive (true);
			StartCoroutine (StopSpeednComboTut ());
		}

		if (gameObject.name == "WallClimbTutorial" && col.gameObject.name == "Character") 
		{
			arrowImage.transform.eulerAngles = new Vector3 (0, 0, 270);
			arrowImage.SetActive (true);
			tutorialText.text = "Swipe Up to climb";
			TutTextGameObject.SetActive (true);
			StartCoroutine (StopWallClimbTut ());
		}
	}

	void TutorialArrownTextDisapper ()
	{
		arrowImage.SetActive (false);
		TutTextGameObject.SetActive (false);
		Time.timeScale = 1f;
	}

	IEnumerator StopWallRunTut ()
	{
		yield return new WaitForSeconds (0.2f);
		Time.timeScale = 1f;
		yield return new WaitForSeconds (2f);
		arrowImage.SetActive (false);
		TutTextGameObject.SetActive (false);
	}

	IEnumerator StopSpeednComboTut ()
	{
		yield return new WaitForSeconds (0.1f);
		Time.timeScale = 1f;
		yield return new WaitForSeconds (2f);
		arrowImage.SetActive (false);
		TutTextGameObject.SetActive (false);
	}

	IEnumerator StopWallClimbTut ()
	{
		yield return new WaitForSeconds (0.1f);
		Time.timeScale = 1f;
		yield return new WaitForSeconds (2f);
		arrowImage.SetActive (false);
		TutTextGameObject.SetActive (false);
	}
}