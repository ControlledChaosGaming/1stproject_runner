﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Script_RP_PowerButtons : MonoBehaviour {

	int multiplier;
	int comboMultiplier;

	bool multiplierBool;

	public bool highJumpTutorial = false;
	public bool midJumpTutorial = false;
	public bool powerSlideTutorial = false ;

	public float multiplierTimer = 2.0f;
	public Slider multiplierSlider;

	public GameObject multiplierSliderToggle;
	public GameObject multiplierTextPlaceHolder;
	public GameObject multiplierTextGo;

	public Text multiplierText;
	public Text ScoreText;

	public int speed1;
	public int speed2;
	public int speed3;

	public Script_RP_Scoring scoring;
	public Script_RP_CoinCollectorNGrounded coinCollectorNGrounded;
	public Script_RP_PlayerBehaviour player;
	public Script_RP_AnimationTriggers animationTriggers;

	// Use this for initialization
	void Start () 
	{
		comboMultiplier = 0;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (multiplierBool == true) 
		{
			multiplierTimer -= Time.deltaTime;
			multiplierSlider.value = multiplierTimer;
			if (multiplierTimer <= 0f) // combo multiplier timer ends
			{
				multiplierBool = false;
				comboMultiplier = 0;
				multiplierSliderToggle.SetActive (false);
			}
		}
	}

	public void HighJump ()//High Jump Button
	{
		highJumpTutorial = true;
		if (animationTriggers.animations.GetCurrentAnimatorStateInfo (0).IsName ("RR_Running")) // if the player is running only then perform action
		{
			if (coinCollectorNGrounded.isGroundedScript == true) {
				animationTriggers.HighJump ();
				ScoreMultiply ();
				if (player.speed == speed1)
					SpeedNSliderSpeed2 ();
				else if (player.speed == speed2)
					SpeedNSliderSpeed3 ();
			}
		}
	}

	public void MidJump ()//Mid Jump Button
	{
		midJumpTutorial = true;
		if (animationTriggers.animations.GetCurrentAnimatorStateInfo (0).IsName ("RR_Running")) // if the player is running only then perform action
		{
			if (coinCollectorNGrounded.isGroundedScript == true) {
				animationTriggers.MidJump ();
				ScoreMultiply ();
				if (player.speed == speed1)
					SpeedNSliderSpeed2 ();
			}
		}
	}

	public void Slide ()//Slide Button
	{
		powerSlideTutorial = true;
		if (animationTriggers.animations.GetCurrentAnimatorStateInfo (0).IsName ("RR_Running")) // if the player is running only then perform action
		{
			if (coinCollectorNGrounded.isGroundedScript == true) {
				animationTriggers.PowerSlide ();
				ScoreMultiply ();
				if (player.speed == speed1)
					SpeedNSliderSpeed2 ();
				else if (player.speed == speed2)
					SpeedNSliderSpeed3 ();
			}
		}
	}

	void ScoreMultiply ()
	{
		multiplierSliderToggle.SetActive (true);
		comboMultiplier += 1;
		multiplierBool = true;
		multiplierTimer = 5f;
		scoring.ScoreMultiplier (comboMultiplier * 100);
		multiplierText.text = "" + comboMultiplier * 100;
		StartCoroutine ("MovingMultiplier");
	}

	IEnumerator MovingMultiplier ()
	{
		multiplierTextGo.transform.position = multiplierTextPlaceHolder.transform.position;
		multiplierTextGo.SetActive (true);
		multiplierText.enabled = true;
		while (Vector3.Distance (multiplierText.transform.position, ScoreText.transform.position) > 0.05f) 
		{
			multiplierText.transform.position = (Vector3.Lerp (multiplierText.transform.position, ScoreText.transform.position, Time.deltaTime * 5f));
			yield return null;
		}
		multiplierTextGo.SetActive(false);	
	}

	void SpeedNSliderSpeed2 ()
	{
 		StartCoroutine (IncreaseSpeedGraduallySpeed2 ());
	}

	IEnumerator IncreaseSpeedGraduallySpeed2( )
	{
		for (int i = speed1; i <= speed2; i++) {
			player.speed = i;	
			player.playerSpeedSlider.value = i;
			yield return new WaitForSeconds (0.05f);
		}
	}

	void SpeedNSliderSpeed3 ()
	{
		StartCoroutine (IncreaseSpeedGraduallySpeed3 ());
	}

	IEnumerator IncreaseSpeedGraduallySpeed3 ()
	{
		for (int i = speed2; i <= speed3; i++) {
			player.speed = i;	
			player.playerSpeedSlider.value = i;
			yield return new WaitForSeconds (0.05f);
		}
	}
}