﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using GooglePlayGames;
using GooglePlayGames.BasicApi;

public class Script_RP_PlayGamesServices : MonoBehaviour {

	// Use this for initialization
	void Start ()
	{
		
	}

	public static void UnlockAchievement(string id)
	{
		Social.ReportProgress (id, 100, success => { });
	}

	public static void IncrementalAchievement (string id, int stepsToIncrement)
	{
		PlayGamesPlatform.Instance.IncrementAchievement (id, stepsToIncrement, success => { });
	}

	public static void ShowAchievementsUI()
	{
		Social.ShowAchievementsUI();
	}

	public static void AddScoreToLeaderboard(string leaderboardId, long score)
	{
		Social.ReportScore (score, leaderboardId, success => { });
	}

	public static void ShowLeaderboardsUI()
	{
		Social.ShowLeaderboardUI ();
	}
}
