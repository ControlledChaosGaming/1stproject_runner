﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerScript : MonoBehaviour {

	public static ManagerScript Instance { get; private set; }
	public static int Counter { get; private set; }

	Script_RP_Scoring scoring;

	// Use this for initialization
	void Start () {
		Instance = this;
	}
	
	public void IncrementCounter()
	{
		scoring = GameObject.Find ("CameraMovement").GetComponent<Script_RP_Scoring> ();
		Script_RP_PlayGamesServices.AddScoreToLeaderboard (RunnerProjectResources.leaderboard_leaderboard1, scoring.currentScore);
	}

	public void RestartGame()
	{
		//Script_RP_PlayGamesServices.AddScoreToLeaderboard (RunnerProjectResources.leaderboard_leaderboard1, Counter);
		Counter = 0;
	}
}
