﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_RP_TitleButtons : MonoBehaviour {

	public GameObject titleCanvas;
	public GameObject loadoutCanvas;
	public GameObject loadouts;
	public GameObject levelSelector;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void LoadoutBtn()
	{
		titleCanvas.SetActive (false);
		loadoutCanvas.SetActive (true);
		loadoutCanvas.SetActive (false);
		loadoutCanvas.SetActive (true);
		loadouts.SetActive (true);
		loadouts.SetActive (false);
		loadouts.SetActive (true);
		levelSelector.SetActive (false);
	}

	public void BackToLevelSelect()
	{
		levelSelector.SetActive (true);
		loadoutCanvas.SetActive (false);
	}

	public void Highjump (int highJumpNo)
	{
		PlayerPrefs.SetInt ("highJumpNo", highJumpNo);
	}

	public void MidJump (int midJumpNo)
	{
		PlayerPrefs.SetInt ("midJumpNo", midJumpNo);
	}

	public void PowerSlide (int powerSlideNo)
	{
		PlayerPrefs.SetInt ("powerSlideNo", powerSlideNo);
	}

	public void StartBtn()
	{
		titleCanvas.SetActive (false);
		levelSelector.SetActive (true);
	}

	public void RestartFromMainGameBtn()
	{
		Application.LoadLevel (Application.loadedLevel);
		Time.timeScale = 1f;
		GameObject.Find ("Preloader").GetComponent<Script_RP_Preloader> ().AdsShow (2);
		AdManager.Instance.HideBanner ();
	}

	public void TutorialLevel()
	{
		Application.LoadLevel ("TutorialLevel");
	}

	public void Authentication()
	{
		
	}
}
