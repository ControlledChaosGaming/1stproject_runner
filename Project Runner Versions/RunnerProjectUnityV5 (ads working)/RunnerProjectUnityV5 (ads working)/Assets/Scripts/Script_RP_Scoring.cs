﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Script_RP_Scoring : MonoBehaviour {

	public Text ScoreDisplay;
	public int currentScore = 0;

	public Script_RP_PlayerBehaviour player;
	public Script_RP_PowerButtons btns;

	// Use this for initialization
	void Start () {
		InvokeRepeating ("ScoreUpdate", 0.05f, 0.05f);
	}

	void Update()
	{
		if (GameObject.Find ("SlidingAnim") == null) 
		{
			CancelInvoke ();
		}


	}
	
	// Update is called once per frame
	void ScoreUpdate () {
		if (player.speed == btns.speed1)
		{
			currentScore += 1;
		}
		if (player.speed > btns.speed1 && player.speed <= btns.speed2) 
		{
			currentScore += 4;
		}
		if (player.speed > btns.speed2)
		{
			currentScore += 10;
		}		
		ScoreDisplay.text = "Score: " + currentScore;
	}

	public void ScoreMultiplier(int multiplier)
	{
		currentScore += multiplier;
	}
}
