﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_RP_Preloader : MonoBehaviour {

	public int adsShow = 0;

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad (gameObject);
		if (Application.loadedLevel == 0) {
			Debug.Log ("Game Started");
			Application.LoadLevel ("Title");
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void AdsShow (int adShowIncrement)
	{
		adsShow += adShowIncrement;
	}
}
