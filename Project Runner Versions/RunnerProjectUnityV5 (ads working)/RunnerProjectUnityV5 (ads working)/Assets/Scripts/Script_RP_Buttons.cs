﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_RP_Buttons : MonoBehaviour {

	public GameObject hud;
	public GameObject pauseMenu;
	public GameObject loadoutWarning;

	Script_RP_Preloader preloader;

	// Use this for initialization
	void Start () {
		preloader = GameObject.Find ("Preloader").GetComponent<Script_RP_Preloader> ();
	}

	public void Pause()
	{
		pauseMenu.SetActive (true);
		hud.SetActive (false);
		Time.timeScale = 0f;
		AdManager.Instance.ShowBanner ();
	}

	public void Resume()
	{	
		hud.SetActive (true);
		pauseMenu.SetActive (false);
		Time.timeScale = 1f;
		AdManager.Instance.HideBanner ();
	}

	public void ExitToMainMenu()
	{
		Application.LoadLevel ("Title");
	}

	public void LoadoutWarning ()
	{
		loadoutWarning.SetActive (true);
		pauseMenu.SetActive (false);
	}

	public void NoBtnWarningLoadout()
	{
		pauseMenu.SetActive(true);
		loadoutWarning.SetActive (false);
	}

	public void PlayAgainBtn()
	{
		Application.LoadLevel (Application.loadedLevel);
		preloader.AdsShow (2);
		Debug.Log ("adshow" + preloader.adsShow);

	}

	public void NextLevel(int levelNo)
	{
		preloader.AdsShow (4);
		Application.LoadLevel ("AlphaLevel" + (levelNo + 1));
	}
}