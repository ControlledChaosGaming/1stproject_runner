﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using admob;

public class AdManager : MonoBehaviour {

	public static AdManager Instance{ set; get;}

	public string bannerId;
	public string videoId;
	Script_RP_Preloader preloader;

	void Start ()
	{
		Instance = this;
		DontDestroyOnLoad (gameObject);
		preloader = GameObject.Find ("Preloader").GetComponent<Script_RP_Preloader> ();

//		Admob.Instance ().initAdmob (bannerId,videoId);
//		Admob.Instance().setTesting(true);
//		Admob.Instance ().loadInterstitial ();
	}

	public void ShowBanner ()
	{
		Debug.Log ("adsShow = " + preloader.adsShow);
		if (preloader.adsShow >= 10) {
			Debug.Log ("adsShow" + preloader.adsShow);
//			Admob.Instance ().showBannerRelative (AdSize.SmartBanner, AdPosition.BOTTOM_CENTER, 0);
		}
	}

	public void HideBanner ()
	{
		Debug.Log ("adsShow = " + preloader.adsShow);
		if (preloader.adsShow >= 10) {
			Debug.Log ("adsShow" + preloader.adsShow);
//			Admob.Instance ().removeAllBanner ();
		}
	}

	public void ShowVideo ()
	{
		Debug.Log ("adsShow = " + preloader.adsShow);
		if (preloader.adsShow >= 10) {
			Debug.Log ("adsShow" + preloader.adsShow);
//			if (Admob.Instance ().isInterstitialReady ()) {
//				Admob.Instance ().showInterstitial ();
//			}
		}
	}
}