﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_RP_CameraMove : MonoBehaviour {

	public GameObject camera;

	Script_RP_PlayerBehaviour player;

	// Use this for initialization
	void Start () {
		player = GameObject.Find ("Character").GetComponent<Script_RP_PlayerBehaviour> ();
	}
	
	// Update is called once per frame
	void Update () {
		camera.transform.Translate (Vector3.forward * player.speed * Time.deltaTime);
		Vector3 pos = camera.transform.position;
		pos.y = 4.5f + player.transform.position.y;
		camera.transform.position = pos;
		//camera.transform.position.y = player.transform.position.y;
	}
}
