﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using admob;

public class Script_RP_DeathTimer : MonoBehaviour {

	public GameObject restartCanvas;

	// Use this for initialization
	void Start () {
	}

	// Update is called once per frame
	void Update () {
		if (GameObject.Find ("SlidingAnim") == null) {
			StartCoroutine (DeathTimer ());
		}
	}

	IEnumerator DeathTimer()
	{
		yield return new WaitForSeconds (1f);
		restartCanvas.SetActive (true);
		Time.timeScale = 0f;
		AdManager.Instance.ShowVideo ();
//		Admob.Instance ().loadInterstitial ();
	}
}